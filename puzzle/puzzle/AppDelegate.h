//
//  AppDelegate.h
//  puzzle
//
//  Created by hongtianjun on 14/11/7.
//  Copyright (c) 2014年 BigBrother. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

