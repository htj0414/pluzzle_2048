//
//  HHTheme.h
//  puzzle
//
//  Created by hongtianjun on 14/12/5.
//  Copyright (c) 2014年 BigBrother. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol HHTheme <NSObject>

/** The background color of the board base. */
+ (UIColor *)boardColor;

/** The background color of the entire scene. */
+ (UIColor *)backgroundColor;

/** The background color of the score board. */
+ (UIColor *)scoreBoardColor;

/** The background color of the button. */
+ (UIColor *)buttonColor;

/** The name of the bold font. */
+ (NSString *)boldFontName;

/** The name of the regular font. */
+ (NSString *)regularFontName;

/**
 * The color for the given level. If level is greater than 15, return the color for Level 15.
 *
 * @param level The level of the tile.
 */
+ (UIColor *)colorForLevel:(NSInteger)level;


/**
 * The text color for the given level. If level is greater than 15, return the color for Level 15.
 *
 * @param level The level of the tile.
 */
+ (UIColor *)textColorForLevel:(NSInteger)level;

@end

#define HHCurrentTheme [HHTheme currentTheme]

@interface HHTheme : NSObject

+ (Class)currentTheme;

@end
