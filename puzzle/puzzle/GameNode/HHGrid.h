//
//  HHGrid.h
//  puzzle
//
//  Created by hongtianjun on 14/11/10.
//  Copyright (c) 2014年 BigBrother. All rights reserved.
//

#import <Foundation/Foundation.h>
#include "HHPos.h"
#include "HHTile.h"

@class HHScene;





typedef void (^IteratorBlock)(HHPos);

@interface HHGrid : NSObject


@property (nonatomic,readonly) NSInteger dimension;
@property (nonatomic,weak) HHScene * scene;

@property (nonatomic,readonly) CGFloat tileSize;
@property (nonatomic,readonly) CGFloat cornerRadius;
@property (nonatomic,readonly) CGFloat borderWidth;

-(instancetype)initWithDimension:(NSInteger)dimension;

-(void)removeAllTilesAnimated:(BOOL)animated;

- (BOOL)hasAvailableCells;

-(void)forEach:(IteratorBlock)block reversed:(BOOL)reversed;

- (HHCell *)cellAtPos:(HHPos)pos;

- (HHTile *)tileAtPos:(HHPos)pos;

- (HHTile *)insertTileAtRandomAvailablePositionWithDelay:(BOOL)delay;

-(NSInteger)horizontalOffset;

-(NSInteger)verticalOffset;

-(CGPoint)locationOfPos:(HHPos)pos;
@end
