//
//  HHTile.h
//  puzzle
//
//  Created by hongtianjun on 14/11/10.
//  Copyright (c) 2014年 BigBrother. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <SpriteKit/SpriteKit.h>
#import "HHPos.h"

@class HHCell;

@interface HHTile : SKShapeNode


@property (nonatomic) NSInteger level;

@property (nonatomic,weak) HHCell * cell;

+(HHTile *)createByCell:(HHCell *)cell size:(CGSize)size;

-(void)commitPendingActions;

-(void)moveToCell:(HHCell *)cell;

-(BOOL)canMergeWithTile:(HHTile *)tile;

-(NSInteger)mergeToTile:(HHTile *)tile;

-(NSInteger)merge3ToTile:(HHTile *)tile;


-(void)removeAnimated:(BOOL)animated;
@end
