//
//  HHTile.m
//  puzzle
//
//  Created by hongtianjun on 14/11/10.
//  Copyright (c) 2014年 BigBrother. All rights reserved.
//

#import "HHTile.h"
#import "HHCell.h"


typedef void (^PendingBlock)(void);

@implementation HHTile {
    SKLabelNode * _label;
    
    NSMutableArray * _pendingActions;
    
    PendingBlock _pendingBlock;
}

#pragma mark - Tile Creation

+(HHTile *)createByCell:(HHCell *)cell size:(CGSize)size {
    HHTile * tile = [[HHTile alloc] initWithSize:size];
    tile.cell = cell;
    return tile;
}


-(instancetype)initWithSize:(CGSize)size {
    if (self = [super init]) {
        
        CGRect rect = CGRectMake(0, 0, size.width, size.height);
        CGPathRef pathRef = CGPathCreateWithRoundedRect(rect, 5, 5, NULL);
        self.path = pathRef;
        CFRelease(pathRef);
        self.lineWidth = 5;
        
        _pendingActions = [[NSMutableArray alloc] init];
        
        _label = [SKLabelNode labelNodeWithFontNamed:[HHCurrentTheme boldFontName]];
        _label.position = CGPointMake(size.width / 2, size.height / 2);
        _label.horizontalAlignmentMode = SKLabelHorizontalAlignmentModeCenter;
        _label.verticalAlignmentMode = SKLabelVerticalAlignmentModeCenter;
        _label.text = @"1";
        [self addChild:_label];

        self.level = 0;
        
        if ([HHCurSetting type] == HHGameTypeFibonacci)
            self.level = arc4random_uniform(100) < 40 ? 1 : 2;
        else
            self.level = arc4random_uniform(100) < 95 ? 1 : 2;
    }
    return self;
}



#pragma mark - 


-(void)removeAnimated:(BOOL)animated {
    if (animated) {
        [_pendingActions addObject:[SKAction scaleTo:0 duration:0.3]];
    }
    [_pendingActions addObject:[SKAction removeFromParent]];
    
    __weak typeof(self) weakself = self;
    _pendingBlock = ^{
        [weakself removeFromParent];
    };
    [self commitPendingActions];
}


-(void)commitPendingActions {
    [self runAction:[SKAction sequence:_pendingBlock] completion:^{
        [_pendingBlock removeAllObjects];
        if (_pendingBlock) {
            _pendingBlock();
            _pendingBlock = nil;
        }
    }];
}

@end
