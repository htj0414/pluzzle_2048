//
//  HHCell.m
//  puzzle
//
//  Created by hongtianjun on 14/11/10.
//  Copyright (c) 2014年 BigBrother. All rights reserved.
//

#import "HHCell.h"
#import "HHTile.h"

@implementation HHCell

-(instancetype)initWithPos:(HHPos)pos; {
    if (self = [super init]) {
        self.pos = pos;
        self.tile = nil;
    }
    return self;
}


-(void)setTile:(HHTile *)tile {
    _tile = tile;
    if (tile) {
        tile.cell = self;
    }
}
@end
