//
//  HHGrid.m
//  puzzle
//
//  Created by hongtianjun on 14/11/10.
//  Copyright (c) 2014年 BigBrother. All rights reserved.
//

#import "HHGrid.h"
#import "HHCell.h"
#import "HHTile.h"
#import "HHGameLogic.h"
#import <objc/runtime.h>

@interface HHGrid ()

@property (nonatomic, readwrite) NSInteger dimension;

@end

@implementation HHGrid {
    NSMutableArray * _grid;
    
    CADisplayLink * _addTileDisplayLink;
}

-(instancetype)initWithDimension:(NSInteger)dimension {
    if (self = [super init]) {
        
        _grid = [[NSMutableArray alloc] init];
        
        for (int i = 0; i < dimension; i++) {
            NSMutableArray * array = [[NSMutableArray alloc] init];
            for (int j = 0; j < dimension; j++) {
                HHCell * cell = [[HHCell alloc] initWithPos:HHPosMake(i, j)];
                [array addObject:cell];
            }
            [_grid addObject:array];
        }
        
        self.dimension = dimension;
    }
    return self;
}




#pragma mark - Iterater

-(void)forEach:(IteratorBlock)block reversed:(BOOL)reversed {
    if (reversed) {
        for (NSInteger i = 0; i < _dimension; i++) {
            for (NSInteger j = 0; j < _dimension; j++) {
                block(HHPosMake(i, j));
            }
        }
    }else {
        for (NSInteger i = _dimension - 1; i >= 0 ; i--) {
            for (NSInteger j = _dimension - 1; j >= 0 ; j--) {
                block(HHPosMake(i, j));
            }
        }
    }
}

# pragma mark - Position helpers

- (HHCell *)cellAtPos:(HHPos)pos
{
    if (pos.x >= self.dimension || pos.y >= self.dimension ||
        pos.x < 0 || pos.y < 0) return nil;
    return [[_grid objectAtIndex:pos.x] objectAtIndex:pos.y];
}


- (HHTile *)tileAtPos:(HHPos)pos
{
    HHCell *cell = [self cellAtPos:pos];
    return cell ? cell.tile : nil;
}

# pragma mark - Cell availability

- (BOOL)hasAvailableCells
{
    return [self availableCells].count != 0;
}


- (HHCell *)randomAvailableCell
{
    NSArray *availableCells = [self availableCells];
    if (availableCells.count) {
        return [availableCells objectAtIndex:arc4random_uniform((int)availableCells.count)];
    }
    return nil;
}

- (NSArray *)availableCells
{
    NSMutableArray *array = [[NSMutableArray alloc] initWithCapacity:self.dimension * self.dimension];
    [self forEach:^(HHPos pos) {
        HHCell *cell = [self cellAtPos:pos];
        if (!cell.tile) [array addObject:cell];
    } reversed:NO];
    return array;
}

- (BOOL)movesAvailable
{
    return [self hasAvailableCells] || [self adjacentMatchesAvailable];
}

- (BOOL)adjacentMatchesAvailable
{
    for (NSInteger i = 0; i < self.dimension; i++) {
        for (NSInteger j = 0; j < self.dimension; j++) {
            // Due to symmetry, we only need to check for tiles to the right and down.
            HHTile * tile = [self tileAtPos:HHPosMake(i, j)];
            
            // Continue with next iteration if the tile does not exist. Note that this means that
            // the cell is empty. For our current usage, it will never happen. It is only in place
            // in case we want to use this function by itself.
            if (!tile) continue;
            
            if (HHCurSetting.type == HHGameTypePowerOf3) {
                if (([tile canMergeWithTile:[self tileAtPos:HHPosMake(i + 1, j)]] &&
                     [tile canMergeWithTile:[self tileAtPos:HHPosMake(i + 2, j)]]) ||
                    ([tile canMergeWithTile:[self tileAtPos:HHPosMake(i, j + 1)]] &&
                     [tile canMergeWithTile:[self tileAtPos:HHPosMake(i, j + 2)]])) {
                        return YES;
                    }
            } else {
                if ([tile canMergeWithTile:[self tileAtPos:HHPosMake(i + 1, j)]] ||
                    [tile canMergeWithTile:[self tileAtPos:HHPosMake(i, j + 1)]]) {
                    return YES;
                }
            }
        }
    }
    
    // Nothing is found.
    return NO;
}

# pragma mark - Position to point conversion

-(NSInteger)horizontalOffset {
    CGFloat width = self.dimension * (self.tileSize + self.borderWidth) + self.borderWidth;
    return ([[UIScreen mainScreen] bounds].size.width - width) / 2;
}


-(NSInteger)verticalOffset {
    CGFloat height = self.dimension * (self.tileSize + self.borderWidth) + self.borderWidth + 120;
    return ([[UIScreen mainScreen] bounds].size.height - height) / 2;
}

-(CGPoint)locationOfPos:(HHPos)pos {
    return CGPointMake([self xLocationOfPos:pos] + self.horizontalOffset,
                       [self yLocationOfPos:pos] + self.verticalOffset);
}


- (CGFloat)xLocationOfPos:(HHPos)pos {
    return pos.y * (self.tileSize + self.borderWidth) + self.borderWidth;
}


- (CGFloat)yLocationOfPos:(HHPos)pos{
    return pos.x * (self.tileSize + self.borderWidth) + self.borderWidth;
}


# pragma mark - Cell manipulation


- (HHTile *)insertTileAtRandomAvailablePositionWithDelay:(BOOL)delay
{
    HHCell *cell = [self randomAvailableCell];
    if (cell) {
        HHTile * tile = [HHTile createByCell:cell size:CGSizeMake(self.tileSize, self.tileSize)];
        tile.position = [self locationOfPos:cell.pos];
        
        SKAction *delayAction = delay ? [SKAction waitForDuration:self.animationDuration * 3] :
        [SKAction waitForDuration:0];
        SKAction *move = [SKAction moveBy:CGVectorMake(- self.tileSize / 2, - self.tileSize / 2)
                                 duration:self.animationDuration];
        SKAction *scale = [SKAction scaleTo:1 duration:self.animationDuration];
        [tile runAction:[SKAction sequence:@[delayAction, [SKAction group:@[move, scale]]]]];
        return tile;
    }
    return nil;
}

-(void)removeAllTilesAnimated:(BOOL)animated {
    [self forEach:^(HHPos pt) {
        HHTile * tile = [self tileAtPos:pt];
        if (tile) [tile removeAnimated:animated];
    } reversed:NO];
}

-(CGFloat)tileSize {
    return self.dimension <= 4 ? 56 : 80;
}

-(CGFloat)cornerRadius {
    return 10.0f;
}

-(CGFloat)borderWidth {
    return 5.0f;
}

-(CGFloat)animationDuration {
    return 0.5f;
}
@end
