//
//  HHCell.h
//  puzzle
//
//  Created by hongtianjun on 14/11/10.
//  Copyright (c) 2014年 BigBrother. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "HHPos.h"

@class HHTile;


@interface HHCell : NSObject

@property (nonatomic) HHPos pos;
@property (nonatomic,strong)HHTile * tile;

-(instancetype)initWithPos:(HHPos)pos;

@end
