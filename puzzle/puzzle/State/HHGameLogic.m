//
//  HHGameLogic.m
//  puzzle
//
//  Created by hongtianjun on 14/12/11.
//  Copyright (c) 2014年 BigBrother. All rights reserved.
//

#import "HHGameLogic.h"

@implementation HHGameLogic


+(NSInteger)winningLevel {
    NSInteger dimension = [HHCurSetting dimension];
    if ([HHCurSetting type] == HHGameTypePowerOf3) {
        switch (dimension) {
            case 3: return 4;
            case 4: return 5;
            case 5: return 6;
            default: return 5;
        }
    }
    
    NSInteger level = 11;
    if (dimension == 3) return level - 1;
    if (dimension == 5) return level + 2;
    return level;
}


+(BOOL)isLevel:(NSInteger)level1 mergeableWithLevel:(NSInteger)level2 {
    if ([HHCurSetting type] == HHGameTypeFibonacci) return abs((int)level1 - (int)level2) == 1;
    return level1 == level2;
}


+(NSInteger)mergeLevel:(NSInteger)level1 withLevel:(NSInteger)level2 {
    if (![self isLevel:level1 mergeableWithLevel:level2]) return 0;
    
    if ([HHCurSetting type] == HHGameTypeFibonacci) {
        return (level1 + 1 == level2) ? level2 + 1 : level1 + 1;
    }
    return level1 + 1;
}


+(NSInteger)valueForLevel:(NSInteger)level {
    if ([HHCurSetting type] == HHGameTypeFibonacci) {
        NSInteger a = 1, b = 1;
        for (NSInteger i = 0; i < level; i++) {
            NSInteger c = a + b;
            a = b;
            b = c;
        }
        return b;
    } else {
        NSInteger value = 1;
        NSInteger base = [HHCurSetting type] == HHGameTypePowerOf2 ? 2 : 3;
        for (NSInteger i = 0; i < level; i++) {
            value *= base;
        }
        return value;
    }
}



@end
