//
//  HHGameManager.h
//  puzzle
//
//  Created by hongtianjun on 14/11/13.
//  Copyright (c) 2014年 BigBrother. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "HHGrid.h"


//typedef NS_ENUM(NSInteger, HHDirection) {
//    HHDirectionUp,
//    HHDirectionLeft,
//    HHDirectionDown,
//    HHDirectionRight
//};


@interface HHGameManager : NSObject

+(HHGameManager *)defaultManager;

-(HHGrid *)gridWithNewSession;

-(void)moveToDirection:(HHDirection)direction;
@end
