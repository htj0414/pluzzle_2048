//
//  HHSetting.h
//  puzzle
//
//  Created by hongtianjun on 14/11/19.
//  Copyright (c) 2014年 BigBrother. All rights reserved.
//

#import <Foundation/Foundation.h>


#define HHCurSetting [HHSetting defaultSetting]
#define HHUserDefault [NSUserDefaults standardUserDefaults]

typedef NS_ENUM(NSInteger, HHGameType) {
    HHGameTypeFibonacci = 2,
    HHGameTypePowerOf2 = 0,
    HHGameTypePowerOf3 = 1
};

extern NSString * const kSettingKeyBorderSize;
extern NSString * const kSettingKeyGameType;
extern NSString * const kSettingKeyTheme;


@interface HHSetting : NSObject

@property (nonatomic, readonly) NSInteger dimension;
@property (nonatomic, readonly) HHGameType type;
@property (nonatomic, readonly) NSString * theme;

+(HHSetting *)defaultSetting;

@end
