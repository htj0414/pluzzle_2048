//
//  HHGameLogic.h
//  puzzle
//
//  Created by hongtianjun on 14/12/11.
//  Copyright (c) 2014年 BigBrother. All rights reserved.
//

#import <Foundation/Foundation.h>


typedef NS_ENUM(NSInteger, HHDirection) {
    HHDirectionUp,
    HHDirectionLeft,
    HHDirectionDown,
    HHDirectionRight
};

@interface HHGameLogic : NSObject

+(NSInteger)winningLevel;
+(BOOL)isLevel:(NSInteger)level1 mergeableWithLevel:(NSInteger)level2;
+(NSInteger)mergeLevel:(NSInteger)level1 withLevel:(NSInteger)level2;
+(NSInteger)valueForLevel:(NSInteger)level;
@end
