//
//  HHPos.h
//  puzzle
//
//  Created by hongtianjun on 14/12/5.
//  Copyright (c) 2014年 BigBrother. All rights reserved.
//

#import <Foundation/Foundation.h>


struct HHPos {
    NSInteger x;
    NSInteger y;
};
typedef struct HHPos HHPos;


CG_INLINE HHPos HHPosMake(NSInteger x, NSInteger y) {
    HHPos pos;
    pos.x = x; pos.y = y;
    return pos;
}
