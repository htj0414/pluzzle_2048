//
//  HHSetting.m
//  puzzle
//
//  Created by hongtianjun on 14/11/19.
//  Copyright (c) 2014年 BigBrother. All rights reserved.
//

#import "HHSetting.h"

NSString * const kSettingKeyBorderSize = @"__border_size";
NSString * const kSettingKeyGameType = @"__game_type";
NSString * const kSettingKeyTheme = @"__theme";



@interface HHSetting()

@property (nonatomic) NSInteger demension;
@property (nonatomic) HHGameType type;
@property (nonatomic) NSString * theme;
@end

@implementation HHSetting


+(HHSetting *)defaultSetting {
    static HHSetting * _setting = nil;
    static dispatch_once_t once;
    dispatch_once(&once, ^{
        _setting = [[self alloc] init];
    });
    return _setting;
}


-(instancetype)init {
    if (self = [super init]) {
        _dimension = [HHUserDefault integerForKey:kSettingKeyBorderSize] + 3;
        _type = [HHUserDefault integerForKey:kSettingKeyGameType];
        _theme = [HHUserDefault stringForKey:kSettingKeyTheme];
    }
    return self;
}



@end
