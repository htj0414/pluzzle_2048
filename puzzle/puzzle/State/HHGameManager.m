//
//  HHGameManager.m
//  puzzle
//
//  Created by hongtianjun on 14/11/13.
//  Copyright (c) 2014年 BigBrother. All rights reserved.
//

#import "HHGameManager.h"
#import "HHGrid.h"

BOOL iterate(NSInteger value, BOOL countUp, NSInteger upper, NSInteger lower) {
    return countUp ? value < upper : value > lower;
}

@implementation HHGameManager {
    /** True if game over. */
    BOOL _over;
    
    /** True if won game. */
    BOOL _won;
    
    /** True if user chooses to keep playing after winning. */
    BOOL _keepPlaying;
    
    /** The current score. */
    NSInteger _score;
    
    /** The points earned by the user in the current round. */
    NSInteger _pendingScore;
    
    /** The grid on which everything happens. */
    HHGrid *_grid;
    
    /** The display link to add tiles after removing all existing tiles. */
    CADisplayLink *_addTileDisplayLink;
}

+(HHGameManager *)defaultManager {
    static HHGameManager * _manager = nil;
    static dispatch_once_t one;
    dispatch_once(&one, ^{
        _manager = [[self alloc] init];
    });
    return _manager;
}


-(HHGrid *)gridWithNewSession {
    if (_grid) [_grid removeAllTilesAnimated:NO];
    
    if (!_grid || _grid.dimension != HHCurSetting.dimension) {
        _grid = [[HHGrid alloc] initWithDimension:HHCurSetting.dimension];
    }
    
    _score = 0; _over = NO; _won = NO; _keepPlaying = NO;
    return _grid;
}

-(void)moveToDirection:(HHDirection)direction {
    __block HHTile *tile = nil;
    
    // Remember that the coordinate system of SpriteKit is the reverse of that of UIKit.
    BOOL reverse = direction == HHDirectionUp || direction == HHDirectionRight;
    NSInteger unit = reverse ? 1 : -1;
    if (direction == HHDirectionUp || direction == HHDirectionDown) {
        [_grid forEach:^(HHPos pos) {
            if ((tile = [_grid tileAtPos:pos])) {
                NSInteger target = pos.x;
                for (NSInteger i = pos.x + unit; iterate(i, reverse, _grid.dimension, -1); i += unit) {
                    HHTile *t = [_grid tileAtPos:HHPosMake(i, pos.y)];
                    
                    // Empty cell; we can move at least to here.
                    if (!t) target = i;
                    
                    // Try to merge to the tile in the cell.
                    else {
                        NSInteger level = 0;
                        
                        if (HHCurSetting.type == HHGameTypePowerOf3) {
                            HHPos further = HHPosMake(i + unit, pos.y);
                            HHTile *ft = [_grid tileAtPos:further];
                            if (ft) {
                                //level = [tile merge3ToTile:t andTile:ft];
                            }
                        } else {
                            level = [tile mergeToTile:t];
                        }
                        
                        if (level) {
                            target = pos.x;
                            //_pendingScore = [GSTATE valueForLevel:level];
                        }
                        
                        break;
                    }
                }
                
                if (target != pos.x) {
                    [tile moveToCell:[_grid cellAtPos:HHPosMake(target, pos.y)]];
                    _pendingScore++;
                }
            }
        } reversed:YES];
    }else {
        [_grid forEach:^(HHPos pos) {
            if ((tile = [_grid tileAtPos:pos])) {
                NSInteger target = pos.y;
                for (NSInteger i = pos.y + unit; iterate(i, reverse, _grid.dimension, -1); i += unit) {
                    HHTile *t = [_grid tileAtPos:HHPosMake(pos.x, i)];
                    
                    if (!t) target = i;
                    
                    else {
                        NSInteger level = 0;
                        
                        if (HHCurSetting.type == HHGameTypePowerOf3) {
                            HHPos further = HHPosMake(pos.x, i + unit);
                            HHTile *ft = [_grid tileAtPos:further];
                            if (ft) {
                                //level = [tile merge3ToTile:t andTile:ft];
                            }
                        } else {
                            level = [tile mergeToTile:t];
                        }
                        
                        if (level) {
                            target = pos.y;
                            //_pendingScore = [GSTATE valueForLevel:level];
                        }
                        
                        break;
                    }
                }
                
                // The current tile is movable.
                if (target != pos.y) {
                    [tile moveToCell:[_grid cellAtPos:HHPosMake(pos.x, target)]];
                    _pendingScore++;
                }
            }
        } reversed:YES];
        
        [self materializePendingScore];
        
        // Check post-move status.
        if (!_keepPlaying && _won) {
            // We set `keepPlaying` to YES. If the user decides not to keep playing,
            // we will be starting a new game, so the current state is no longer relevant.
            _keepPlaying = YES;
            //[_grid.scene.controller endGame:YES];
        }
        
        // Add one more tile to the grid.
        [_grid insertTileAtRandomAvailablePositionWithDelay:YES];
        if (HHCurSetting.dimension == 5 && HHCurSetting.type == HHGameTypePowerOf2)
            [_grid insertTileAtRandomAvailablePositionWithDelay:YES];
        
        if (![self movesAvailable]) {
            //[_grid.scene.controller endGame:NO];
        }
    }
}

- (void)materializePendingScore
{
    _score += _pendingScore;
    _pendingScore = 0;
    //[_grid.scene.controller updateScore:_score];
}

- (BOOL)movesAvailable
{
    return [_grid hasAvailableCells] || [self adjacentMatchesAvailable];
}

- (BOOL)adjacentMatchesAvailable
{
    for (NSInteger i = 0; i < _grid.dimension; i++) {
        for (NSInteger j = 0; j < _grid.dimension; j++) {
            // Due to symmetry, we only need to check for tiles to the right and down.
            HHTile * tile = [_grid tileAtPos:HHPosMake(i, j)];
            
            // Continue with next iteration if the tile does not exist. Note that this means that
            // the cell is empty. For our current usage, it will never happen. It is only in place
            // in case we want to use this function by itself.
            if (!tile) continue;
            
            if (HHCurSetting.type == HHGameTypePowerOf3) {
                if (([tile canMergeWithTile:[_grid tileAtPos:HHPosMake(i + 1, j)]] &&
                     [tile canMergeWithTile:[_grid tileAtPos:HHPosMake(i + 2, j)]]) ||
                    ([tile canMergeWithTile:[_grid tileAtPos:HHPosMake(i, j + 1)]] &&
                     [tile canMergeWithTile:[_grid tileAtPos:HHPosMake(i, j + 2)]])) {
                        return YES;
                    }
            } else {
                if ([tile canMergeWithTile:[_grid tileAtPos:HHPosMake(i + 1, j)]] ||
                    [tile canMergeWithTile:[_grid tileAtPos:HHPosMake(i, j + 1)]]) {
                    return YES;
                }
            }
        }
    }
    
    // Nothing is found.
    return NO;
}
@end
