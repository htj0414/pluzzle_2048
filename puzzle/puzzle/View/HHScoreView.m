//
//  HHScoreView.m
//  puzzle
//
//  Created by hongtianjun on 14/11/7.
//  Copyright (c) 2014年 BigBrother. All rights reserved.
//

#import "HHScoreView.h"

@implementation HHScoreView

-(id)initWithFrame:(CGRect)frame {
    if (self = [super initWithFrame:frame]) {
        
        self.layer.masksToBounds = YES;
        self.layer.cornerRadius = 5.5;
        self.backgroundColor = [UIColor orangeColor];
        self.layer.shadowOpacity = 0.5f;
        self.layer.shadowColor = [[UIColor blackColor] CGColor];
        self.layer.shadowOffset = CGSizeMake(1, 1);
        
        
        self.titleLabel = [[UILabel alloc] initWithFrame:CGRectZero];
        self.titleLabel.textColor = [UIColor whiteColor];
        self.titleLabel.textAlignment = NSTextAlignmentCenter;
        self.titleLabel.font = [UIFont systemFontOfSize:25];
        self.titleLabel.text = @"Score";
        self.titleLabel.backgroundColor = [UIColor clearColor];
        [self addSubview:self.titleLabel];
        
        self.scoreLabel = [[UILabel alloc] initWithFrame:CGRectZero];
        self.scoreLabel.font = [UIFont boldSystemFontOfSize:60];
        self.scoreLabel.adjustsFontSizeToFitWidth = YES;
        self.scoreLabel.minimumScaleFactor = 0.1f;
        self.scoreLabel.textColor = [UIColor whiteColor];
        self.scoreLabel.textAlignment = NSTextAlignmentCenter;
        self.scoreLabel.backgroundColor = [UIColor clearColor];
        self.scoreLabel.text = @"998";
        [self addSubview:self.scoreLabel];
        
        
        [self.titleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(self).with.offset(5);
            make.left.equalTo(self).with.offset(5);
            make.right.equalTo(self).with.offset(-5);
            make.height.mas_equalTo(20);
        }];
        
        [self.scoreLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(self.titleLabel.mas_bottom).with.offset(5);
            make.left.equalTo(self).with.offset(5);
            make.right.equalTo(self).with.offset(-5);
            make.bottom.equalTo(self).with.offset(-5);
        }];
    }
    return self;
}

-(void)setTextColor:(UIColor *)textColor {
    self.titleLabel.textColor = textColor;
    self.scoreLabel.textColor = textColor;
}


//-(void)setBackgroundColor:(UIColor *)backgroundColor {
//    [super setBackgroundColor:backgroundColor];
//    _backgroundColor = backgroundColor;
//}

@end

