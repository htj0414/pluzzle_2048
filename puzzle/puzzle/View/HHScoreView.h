//
//  HHScoreView.h
//  puzzle
//
//  Created by hongtianjun on 14/11/7.
//  Copyright (c) 2014年 BigBrother. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface HHScoreView : UIView

@property (nonatomic,strong) UILabel * titleLabel;
@property (nonatomic,strong) UILabel * scoreLabel;

@property (nonatomic,strong) UIFont * font UI_APPEARANCE_SELECTOR;
@property (nonatomic,strong) UIColor * textColor UI_APPEARANCE_SELECTOR;

@end

