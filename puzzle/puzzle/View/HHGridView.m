//
//  HHGridView.m
//  puzzle
//
//  Created by hongtianjun on 14/11/10.
//  Copyright (c) 2014年 BigBrother. All rights reserved.
//

#import "HHGridView.h"
#import "HHGrid.h"

@implementation HHGridView

-(instancetype)initWithFrame:(CGRect)frame {
    if (self = [super initWithFrame:frame]) {
//        self.backgroundColor = [HHCurrentTheme b];
//        self.layer.cornerRadius = [HHCurrentTheme cornerRadius];
        self.layer.masksToBounds = YES;
    }
    return self;
}

-(instancetype)init {
    if (self = [super init]) {
        
//        NSInteger side = HHCurSetting.dimension * (GSTATE.tileSize + GSTATE.borderWidth) + GSTATE.borderWidth;
//        CGFloat verticalOffset = [[UIScreen mainScreen] bounds].size.height - GSTATE.verticalOffset;
//        return [self initWithFrame:CGRectMake(GSTATE.horizontalOffset, verticalOffset - side, side, side)];
    }
    return self;
}

+(UIImage *)gridImageWithGrid:(HHGrid *)grid {
    UIView * backgroundView = [[UIView alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
    backgroundView.backgroundColor = [UIColor clearColor];
    backgroundView.opaque = NO;
    
    HHGridView * gridView = [[HHGridView alloc] init];
    [backgroundView addSubview:gridView];
    
    [grid forEach:^(HHPos pos) {
        CALayer * layer = [CALayer layer];
        CGPoint pt = CGPointZero;//[self locationOsPos:pos];
        
        CGRect frame = layer.frame;
        frame.size = CGSizeMake(grid.tileSize, grid.tileSize);
        frame.origin = CGPointMake(pt.x, [[UIScreen mainScreen] bounds].size.height - pt.y - grid.tileSize);
        layer.frame = frame;
        
        layer.backgroundColor = [HHCurrentTheme boardColor].CGColor;
        layer.cornerRadius = grid.cornerRadius;
        layer.masksToBounds = YES;
        [backgroundView.layer addSublayer:layer];
    } reversed:NO];
    return nil;
}

+(UIImage *)gridImageWithOverlay {
    UIView * backgroundView = [[UIView alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
    backgroundView.backgroundColor = [UIColor clearColor];
    backgroundView.opaque = NO;
    
    HHGridView * gridView = [[HHGridView alloc] init];
    //view.backgroundColor = [[HHCurrentTheme backgroundColor] colorWithAlphaComponent:0.8f];
    [backgroundView addSubview:gridView];
    
    return [HHGridView snapshotWithView:backgroundView];
}


+(UIImage *)snapshotWithView:(UIView *)view {
    UIGraphicsBeginImageContextWithOptions(view.frame.size, view.opaque, 0.0f);
    [view.layer renderInContext:UIGraphicsGetCurrentContext()];
    UIImage * image = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return image;
}



@end
