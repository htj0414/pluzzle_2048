//
//  HHTitleView.h
//  puzzle
//
//  Created by hongtianjun on 14/11/17.
//  Copyright (c) 2014年 BigBrother. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface HHTitleView : UIView

@property (nonatomic,strong) UILabel * titleLabel;
@property (nonatomic,strong) UILabel * subtitleLabel;

@end
