//
//  HHTitleView.m
//  puzzle
//
//  Created by hongtianjun on 14/11/17.
//  Copyright (c) 2014年 BigBrother. All rights reserved.
//

#import "HHTitleView.h"

@implementation HHTitleView {
    
}

-(id)initWithFrame:(CGRect)frame {
    if (self = [super initWithFrame:frame]) {
        
        self.backgroundColor = [UIColor clearColor];
        
        self.titleLabel = [[UILabel alloc] initWithFrame:CGRectZero];
        self.titleLabel.font = [UIFont boldSystemFontOfSize:60];
        self.titleLabel.adjustsFontSizeToFitWidth = YES;
        self.titleLabel.minimumScaleFactor = 0.1f;
        self.titleLabel.textColor = [UIColor orangeColor];
        self.titleLabel.text = @"6799";
        self.titleLabel.backgroundColor = [UIColor clearColor];
        
        [self addSubview:self.titleLabel];
        
        self.subtitleLabel = [[UILabel alloc] initWithFrame:CGRectZero];
        self.subtitleLabel.numberOfLines = 0;
        self.subtitleLabel.textColor = [UIColor orangeColor];
        self.subtitleLabel.font = [UIFont systemFontOfSize:20];
        self.subtitleLabel.backgroundColor = [UIColor clearColor];
        self.subtitleLabel.text = @"kla lak  lakd lasd lasd lasd  lkl";
        
        [self addSubview:self.subtitleLabel];
        
        
        [self.titleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.width.equalTo(self);
            make.top.equalTo(self);
            make.left.equalTo(self);
        }];
        [self.subtitleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.width.equalTo(self);
            make.left.equalTo(self);
            make.top.equalTo(self.titleLabel.mas_bottom).with.offset(10);
        }];
    }
    return self;
}

@end
