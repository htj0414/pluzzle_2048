//
//  GameScene.m
//  puzzle
//
//  Created by hongtianjun on 14/11/7.
//  Copyright (c) 2014年 BigBrother. All rights reserved.
//

#import "GameScene.h"
#import "HHGameManager.h"
#import "HHGrid.h"

#define EFFECTIVE_SWIPE_DISTANCE_THRESHOLD 20.0f


#define VALID_SWIPE_DIRECTION_THRESHOLD 2.0f

@implementation GameScene {
    /** The game manager that controls all the logic of the game. */
    HHGameManager *_manager;
    
    BOOL _hasPendingSwipe;
    
    SKSpriteNode * _board;
    
    HHGrid * _grid;
    
    CADisplayLink * _addTileDisplayLink;
    
    NSInteger _score;
    BOOL _over;
    BOOL _won;
    BOOL _keepPlaying;
}

-(instancetype)initWithSize:(CGSize)size {
    if (self = [super initWithSize:size]) {
        _manager = [[HHGameManager alloc] init];
    }
    return self;
}

-(void)didMoveToView:(SKView *)view {
    
    self.backgroundColor = [HHCurrentTheme backgroundColor];
    
    if (view == self.view) {
        UIPanGestureRecognizer * panGesture = [[UIPanGestureRecognizer alloc] initWithTarget:self action:@selector(handleSwipe:)];
        [self.view addGestureRecognizer:panGesture];
    }else {
        for (UIGestureRecognizer * gesture in self.view.gestureRecognizers) {
            [self.view removeGestureRecognizer:gesture];
        }
    }
    
    [self startNewGame];
}


-(void)createBoardWithGrid:(HHGrid *)grid {
    
    if (_board) [_board removeFromParent];
    
    UIImage * image = [self gridImage];
    SKTexture * backgroundTexture = [SKTexture textureWithImage:image];
    _board = [SKSpriteNode spriteNodeWithTexture:backgroundTexture];
    [_board setScale:1.0f];
    [_board setPosition:CGPointMake(CGRectGetMidX(self.frame), CGRectGetMidY(self.frame))];
    [self addChild:_board];
}

-(void)startNewGame {
    
    if (_grid) [_grid removeAllTilesAnimated:NO];
    
    if (!_grid || _grid.dimension != HHCurSetting.dimension) {
        _grid = [[HHGrid alloc] initWithDimension:HHCurSetting.dimension];
    }
    [self createBoardWithGrid:_grid];
    
    // Set the initial state for the game.
    _score = 0; _over = NO; _won = NO; _keepPlaying = NO;
    
    for (int i = 0; i < 2; i++) {
        id cell = [_grid insertTileAtRandomAvailablePositionWithDelay:NO];
        [self addChild:cell];
    }
}


#pragma mark - Image View

-(UIImage *)gridImage {
    UIView * backgroundView = [[UIView alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
    backgroundView.backgroundColor = [UIColor clearColor];
    backgroundView.opaque = NO;
    
    
    NSInteger side = _grid.dimension * (_grid.tileSize + _grid.borderWidth) + _grid.borderWidth;
    CGFloat verticalOffset = [[UIScreen mainScreen] bounds].size.height - _grid.verticalOffset;
    UIView * gridView = [[UIView alloc] initWithFrame:CGRectMake(_grid.horizontalOffset, verticalOffset - side, side, side)];
    [backgroundView addSubview:gridView];
    
    [_grid forEach:^(HHPos pos) {
        CALayer * layer = [CALayer layer];
        CGPoint pt = [_grid locationOfPos:pos];
        
        CGRect frame = layer.frame;
        frame.size = CGSizeMake(_grid.tileSize, _grid.tileSize);
        frame.origin = CGPointMake(pt.x, [[UIScreen mainScreen] bounds].size.height - pt.y - _grid.tileSize);
        layer.frame = frame;
        
        layer.backgroundColor = [HHCurrentTheme boardColor].CGColor;
        layer.cornerRadius = _grid.cornerRadius;
        layer.masksToBounds = YES;
        [backgroundView.layer addSublayer:layer];
    } reversed:NO];
    return [self snapshotWithView:backgroundView];
}

-(UIImage *)gridImageWithOverlay {
    UIView * backgroundView = [[UIView alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
    backgroundView.backgroundColor = [UIColor clearColor];
    backgroundView.opaque = NO;
    
    NSInteger side = _grid.dimension * (_grid.tileSize + _grid.borderWidth) + _grid.borderWidth;
    CGFloat verticalOffset = [[UIScreen mainScreen] bounds].size.height - _grid.verticalOffset;
    UIView * gridView = [[UIView alloc] initWithFrame:CGRectMake(_grid.horizontalOffset, verticalOffset - side, side, side)];
    [backgroundView addSubview:gridView];
    //view.backgroundColor = [[HHCurrentTheme backgroundColor] colorWithAlphaComponent:0.8f];
    
    return [self snapshotWithView:backgroundView];
}


-(UIImage *)snapshotWithView:(UIView *)view {
    UIGraphicsBeginImageContextWithOptions(view.frame.size, view.opaque, 0.0f);
    [view.layer renderInContext:UIGraphicsGetCurrentContext()];
    UIImage * image = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return image;
}


#pragma mark - Gesture

-(void)handleSwipe:(UIPanGestureRecognizer *)gesture {
    if (gesture.state == UIGestureRecognizerStateBegan) {
        _hasPendingSwipe = YES;
    }else if (gesture.state == UIGestureRecognizerStateChanged) {
        [self commitTranslation:[gesture translationInView:self.view]];
    }
}

-(void)commitTranslation:(CGPoint)translation {
    if (!_hasPendingSwipe) return;
    
    CGFloat absX = fabs(translation.x);
    CGFloat absY = fabs(translation.y);
    
    if (absX > absY * VALID_SWIPE_DIRECTION_THRESHOLD) {
        translation.x < 0 ? [_manager moveToDirection:HHDirectionLeft] :
        [_manager moveToDirection:HHDirectionRight];
    } else if (absY > absX * VALID_SWIPE_DIRECTION_THRESHOLD) {
        translation.y < 0 ? [_manager moveToDirection:HHDirectionUp] :
        [_manager moveToDirection:HHDirectionDown];
    }
    
    _hasPendingSwipe = NO;
}

@end
