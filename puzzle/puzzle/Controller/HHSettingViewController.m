//
//  HHSettingViewController.m
//  puzzle
//
//  Created by hongtianjun on 14/12/5.
//  Copyright (c) 2014年 BigBrother. All rights reserved.
//

#import "HHSettingViewController.h"
#import "HHSettingDetailViewController.h"

@implementation HHSettingViewController {
    NSArray *_options;
    NSArray *_optionKeys;
    NSArray *_optionSelections;
    NSArray *_optionsNotes;
}


# pragma mark - Set up

- (instancetype)init
{
    if (self = [super initWithStyle:UITableViewStyleGrouped]) {
        [self commonInit];
    }
    return self;
}


- (instancetype)initWithCoder:(NSCoder *)aDecoder
{
    if (self = [super initWithCoder:aDecoder]) {
        [self commonInit];
    }
    return self;
}


- (void)commonInit
{
    _options = @[@"Game Type", @"Board Size", @"Theme"];
    
    _optionKeys = @[kSettingKeyBorderSize, kSettingKeyGameType,kSettingKeyTheme];
    
    _optionSelections = @[@[@"Powers of 2", @"Powers of 3", @"Fibonacci"],
                          @[@"3 x 3", @"4 x 4", @"5 x 5"],
                          @[@"Default", @"Vibrant", @"Joyful"]];
    
    _optionsNotes = @[@"For Fibonacci games, a tile can be joined with a tile that is one level above or below it, but not to one equal to it. For Powers of 3, you need 3 consecutive tiles to be the same to trigger a merge!",
                      @"The smaller the board is, the harder! For 5 x 5 board, two tiles will be added every round if you are playing Powers of 2.",
                      @"Choose your favorite appearance and get your own feeling of 2048! More (and higher quality) themes are in the works so check back regularly!"];
}


- (void)viewDidLoad
{
    
    
    [super viewDidLoad];
    self.navigationController.navigationBar.tintColor = [HHCurrentTheme scoreBoardColor];
    // Do any additional setup after loading the view.
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self.tableView reloadData];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

# pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 2;
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return section == 0 ? 1 : _options.count;
}


- (NSString *)tableView:(UITableView *)tableView titleForFooterInSection:(NSInteger)section
{
    if (section) return @"";
    return @"Please note: Changing the settings above would restart the game.";
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"Settings Cell"];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:@"Settings Cell"];
        
    }
    if (indexPath.section == 0) {
        cell.accessoryType = UITableViewCellAccessoryNone;
        cell.textLabel.text = @"About 2048";
        cell.detailTextLabel.text = @"1.0";
    } else {
        cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
        cell.textLabel.text = [_options objectAtIndex:indexPath.row];
        
        NSString * key = [_optionKeys objectAtIndex:indexPath.row];
        NSInteger index = [[HHCurSetting valueForKey:key] integerValue];
        cell.detailTextLabel.text = [[_optionSelections objectAtIndex:indexPath.row] objectAtIndex:index];
        cell.detailTextLabel.textColor = [HHCurrentTheme scoreBoardColor];
    }
    
    return cell;
}


- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.section > 0) {
        HHSettingDetailViewController *viewController = [[HHSettingDetailViewController alloc] init];
        
        NSInteger index = [self.tableView indexPathForSelectedRow].row;
        viewController.title = [_options objectAtIndex:index];
        viewController.options = [_optionSelections objectAtIndex:index];
        viewController.footer = [_optionsNotes objectAtIndex:index];
        viewController.key = [_optionKeys objectAtIndex:indexPath.row];
        [self.navigationController pushViewController:viewController animated:YES];
    }
}@end
