//
//  HHSettingDetailViewController.h
//  puzzle
//
//  Created by hongtianjun on 14/12/5.
//  Copyright (c) 2014年 BigBrother. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface HHSettingDetailViewController : UITableViewController

@property (nonatomic, strong) NSString * key;
@property (nonatomic, strong) NSArray * options;
@property (nonatomic, strong) NSString * footer;

@end
