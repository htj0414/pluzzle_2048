//
//  HHSettingDetailViewController.m
//  puzzle
//
//  Created by hongtianjun on 14/12/5.
//  Copyright (c) 2014年 BigBrother. All rights reserved.
//

#import "HHSettingDetailViewController.h"

@implementation HHSettingDetailViewController
- (instancetype)initWithStyle:(UITableViewStyle)style {
    self = [super initWithStyle:UITableViewStyleGrouped];
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.navigationController.navigationBar.tintColor = [HHCurrentTheme scoreBoardColor];
}


# pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.options.count;
}

- (NSString *)tableView:(UITableView *)tableView titleForFooterInSection:(NSInteger)section {
    return self.footer;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"Settings Detail Cell"];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:@"Settings Cell"];
    }
    cell.textLabel.text = [self.options objectAtIndex:indexPath.row];
    NSInteger index = [[HHCurSetting valueForKey:_key] integerValue];
    cell.accessoryType = (index == indexPath.row) ?UITableViewCellAccessoryCheckmark : UITableViewCellAccessoryNone;
    cell.tintColor = [HHCurrentTheme scoreBoardColor];
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [HHCurSetting setValue:[NSNumber numberWithInteger:indexPath.row] forKey:_key];
    [self.tableView reloadData];
    //HHSetting.needRefresh = YES;
}
@end
