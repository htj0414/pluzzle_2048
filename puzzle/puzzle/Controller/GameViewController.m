//
//  GameViewController.m
//  puzzle
//
//  Created by hongtianjun on 14/11/7.
//  Copyright (c) 2014年 BigBrother. All rights reserved.
//

#import "GameViewController.h"
#import "GameScene.h"
#import "HHScoreView.h"
#import "HHTitleView.h"
#import "HHSettingViewController.h"

@implementation SKScene (Unarchive)

+ (instancetype)unarchiveFromFile:(NSString *)file {
    /* Retrieve scene file path from the application bundle */
    NSString *nodePath = [[NSBundle mainBundle] pathForResource:file ofType:@"sks"];
    /* Unarchive the file to an SKScene object */
    NSData *data = [NSData dataWithContentsOfFile:nodePath
                                          options:NSDataReadingMappedIfSafe
                                            error:nil];
    NSKeyedUnarchiver *arch = [[NSKeyedUnarchiver alloc] initForReadingWithData:data];
    [arch setClass:self forClassName:@"SKScene"];
    SKScene *scene = [arch decodeObjectForKey:NSKeyedArchiveRootObjectKey];
    [arch finishDecoding];
    
    return scene;
}

@end

@implementation GameViewController {
    HHTitleView * _titleView;
    HHScoreView * _scoreView;
    HHScoreView * _bestView;
    
    UIButton * _settingButton;
    UIButton * _resetButton;
}

- (void)viewDidLoad
{
    [super viewDidLoad];

    // Configure the view.
    SKView * skView = (SKView *)self.view;
    skView.showsFPS = NO;
    skView.showsNodeCount = NO;
    /* Sprite Kit applies additional optimizations to improve rendering performance */
    skView.ignoresSiblingOrder = YES;
    
    // Create and configure the scene.
    GameScene *scene = [GameScene unarchiveFromFile:@"GameScene"];
    scene.scaleMode = SKSceneScaleModeAspectFill;
    
    // Present the scene.
    [skView presentScene:scene];
    
    //[[HHScoreView appearance] setTextColor:[UIColor redColor]];
    //[[HHScoreView appearance] setBackgroundColor:[UIColor yellowColor]];
    
    _scoreView = [[HHScoreView alloc] initWithFrame:CGRectZero];
    [self.view addSubview:_scoreView];
    
    _bestView = [[HHScoreView alloc] initWithFrame:CGRectZero];
    [self.view addSubview:_bestView];
    
    _titleView = [[HHTitleView alloc] initWithFrame:CGRectZero];
    [self.view addSubview:_titleView];
    
    _settingButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [_settingButton setBackgroundColor:[UIColor redColor]];
    [_settingButton setTitle:@"Setting" forState:UIControlStateNormal];
    [_settingButton.layer setCornerRadius:5.0f];
    [_settingButton.layer setMasksToBounds:YES];
    [_settingButton addTarget:self action:@selector(setting:) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:_settingButton];
    
    _resetButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [_resetButton setBackgroundColor:[UIColor redColor]];
    [_resetButton setTitle:@"Reset" forState:UIControlStateNormal];
    [_resetButton.layer setCornerRadius:5.0f];
    [_resetButton.layer setMasksToBounds:YES];
    [_resetButton addTarget:self action:@selector(reset:) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:_resetButton];
    

    [self regurConstraints];
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation {
    return YES;
}

- (void)willRotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation duration:(NSTimeInterval)duration {
    if (toInterfaceOrientation == UIInterfaceOrientationPortrait || toInterfaceOrientation == UIInterfaceOrientationPortraitUpsideDown) {
        [self compactConstraints];
    }else {
        [self regurConstraints];
    }
    [self.view setNeedsLayout];
}


- (BOOL)shouldAutorotate
{
    return YES;
}

- (NSUInteger)supportedInterfaceOrientations
{
    return UIInterfaceOrientationMaskAll;
}


-(void)willTransitionToTraitCollection:(UITraitCollection *)newCollection withTransitionCoordinator:(id<UIViewControllerTransitionCoordinator>)coordinator {
    [super willTransitionToTraitCollection:newCollection
                 withTransitionCoordinator:coordinator];
    
    [coordinator animateAlongsideTransition:^(id<UIViewControllerTransitionCoordinatorContext> context) {
        if (newCollection.verticalSizeClass == UIUserInterfaceSizeClassCompact) {
            [self compactConstraints];
        }else {
            [self regurConstraints];
        }
    } completion:^(id<UIViewControllerTransitionCoordinatorContext> context) {
        [self.view setNeedsLayout];
    }];
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Release any cached data, images, etc that aren't in use.
}

- (BOOL)prefersStatusBarHidden {
    return YES;
}

-(void)setting:(id)sender {
    HHSettingViewController * viewController = [[HHSettingViewController alloc] init];
    UINavigationController * navController = [[UINavigationController alloc] initWithRootViewController:viewController];
    [self presentViewController:navController animated:YES completion:nil];
}

-(void)reset:(id)sender {
    
}


#pragma mark - Transition

-(void)regurConstraints {
    [_scoreView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.size.mas_equalTo(CGSizeMake(100, 100));
        
        make.top.equalTo(self.view).with.offset(10);
        make.right.equalTo(self.view).with.offset(-10);
    }];
    
    [_bestView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.size.mas_equalTo(CGSizeMake(100, 100));
        
        make.top.equalTo(_scoreView);
        make.right.equalTo(_scoreView.mas_left).with.offset(-10);
    }];
    
    [_settingButton mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.width.equalTo(_scoreView);
        make.left.equalTo(_scoreView);
        make.top.equalTo(_scoreView.mas_bottom).with.offset(10);
    }];
    
    [_resetButton mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.width.equalTo(_bestView);
        make.left.equalTo(_bestView);
        make.top.equalTo(_bestView.mas_bottom).with.offset(10);
    }];
    
    [_titleView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.view).with.offset(10);
        make.top.equalTo(self.view).with.offset(10);
        make.right.equalTo(_bestView.mas_left).with.offset(-10);
        make.bottom.equalTo(_resetButton.mas_bottom);
    }];
}

-(void)compactConstraints {
    [_scoreView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.size.mas_equalTo(CGSizeMake(100, 100));
        
        make.top.equalTo(self.view).with.offset(10);
        make.right.equalTo(self.view).with.offset(-10);
    }];
    
    [_bestView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.size.mas_equalTo(CGSizeMake(100, 100));
        
        make.right.equalTo(_scoreView);
        make.top.equalTo(_scoreView.mas_bottom).with.offset(10);
    }];
    
    
    
    [_settingButton mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.width.equalTo(_bestView);
        make.left.equalTo(_bestView);
        make.top.equalTo(_bestView.mas_bottom).with.offset(10);
    }];
    
    [_resetButton mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.width.equalTo(_settingButton);
        make.left.equalTo(_settingButton);
        make.top.equalTo(_settingButton.mas_bottom).with.offset(10);
    }];
    
    [_titleView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.view).with.offset(10);
        make.top.equalTo(self.view).with.offset(10);
        make.width.equalTo(_bestView.mas_width);
        make.bottom.equalTo(self.view);
    }];
}
@end
